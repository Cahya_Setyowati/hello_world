import 'package:flutter/material.dart';
import 'products.dart';
import './product_control.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;

  ProductManager({this.startingProduct = "Food Tester"});

  @override
  _ProductManagerState createState(){
    print('[ProductManagerState] createState()');
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager> {
  List<String> _products = [];

  void iniState() {
    print('[ProductManagerState] iniState()');
    _products.add(widget.startingProduct);
    super.initState();
  }

  void didUpdateWigdet(ProductManager oldWidget) {
    print('[ProductManager State] didUpdateWidget()');
    super.didUpdateWidget(oldWidget);
  }

  void _addProduct(String product){
    setState(() {
           _products.add(product);
           print(_products);
        });
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductManagerState] bulid ()');
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10.0),
          child: ProductControl(_addProduct),
        ),
      Products(_products),
      ],
    );
  }
}
